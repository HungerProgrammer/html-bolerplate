var gulp = require('gulp'),
	concat = require('gulp-concat'),
	watch = require('gulp-watch'),
	autoprefixer = require('gulp-autoprefixer'),
	rewritecss = require('gulp-rewrite-css'),
	sourcemaps = require('gulp-sourcemaps'),
	sass = require('gulp-sass');

var scripts = [
	'node_modules/jquery/dist/jquery.js',
	'node_modules/tether/dist/js/tether.js',
	'node_modules/bootstrap/dist/js/bootstrap.js',
	'js/*.js'
];

gulp.task('script', function () {
		var stream = gulp.src(scripts, {base: '/'});

		return stream
			.pipe(sourcemaps.init())
			.pipe(concat('app.js'))
			.pipe(sourcemaps.write('./', {includeContent: false, sourceRoot: '/'}))
			.pipe(gulp.dest('./'));
	}
);

gulp.task('scss', function () {
	var stream = gulp.src(['scss/main.scss'])
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError));

	return stream
		.pipe(rewritecss({destination: './'}))
		.pipe(autoprefixer('last 10 versions', 'ie 9'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./'));
});

gulp.task('watch', function () {
	gulp.watch('scss/*.less', ['scss']);
	gulp.watch('js/*.js', ['script']);
});

gulp.task('default', ['scss', 'script', 'watch']);
